import React from 'react';
import ReactDOM from 'react-dom';
import { PersistGate } from 'redux-persist/integration/react'
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Login from './components/Login';
import { BrowserRouter, Route } from 'react-router-dom';
import Articles from './components/Article/Articles';
import ArticleHomme from './components/Article/ArticleHomme';
import ArticleFemme from './components/Article/ArticleFemme';
import ListesArticles from './components/Article/ListesArticles';
import VueArticle from './components/Article/VueArticle';
import Panier from './components/Achat/Panier.js';
import buidlStore from './redux/Store';
import { Provider } from 'react-redux';

const { store, persistor } = buidlStore();

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <React.StrictMode>
        <BrowserRouter>
          <Route exact path="/" component={App} />
          <Route path="/login" component={Login} />
          <Route path="/articles" component={Articles} />
          <Route path="/panier" component={Panier} />
          <Route path="/listesArticle" component={ListesArticles} />
          <Route path="/articlesHomme" component={ArticleHomme} />
          <Route path="/articleFemme" component={ArticleFemme} />
          <Route path="/vueArticle" component={VueArticle} />
        </BrowserRouter>
      </React.StrictMode>
    </PersistGate>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
