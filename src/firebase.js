import { useCallback, useEffect } from 'react'

import firebase from "firebase";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import { logInWithEmailPassword, logOut } from './redux/userSlice';
import { cleanPanier } from "./redux/panierSlice";


// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDxEqI_DfYoGzz88uCgCGnWhbTg7raYcdg",
  authDomain: "mabashop-35f84.firebaseapp.com",
  projectId: "mabashop-35f84",
  storageBucket: "mabashop-35f84.appspot.com",
  messagingSenderId: "1053472830066",
  appId: "1:1053472830066:web:99e0b3282f18e1581b88eb",
  measurementId: "G-ESGRS7WLRW"
};

const app = firebase.initializeApp(firebaseConfig);
const auth = app.auth();
const db = app.firestore();
const googleProvider = new firebase.auth.GoogleAuthProvider();

export const useFirebaseFunction = () => {
  const history = useHistory();

  ////////////////////// dispatch 
  const dispatch = useDispatch();

  //se connecter avec un email et un password 
  const signInWithEmailAndPassword = async (email, password) => {
    try {
      const { user } = await auth.signInWithEmailAndPassword(email, password);
      dispatch(logInWithEmailPassword({ email: user.email, name: undefined }));
      //redirection link
      history.push("/");
    } catch (err) {
      console.error(err);
      alert(err.message);
    }
  };

  //se connecter avec google 
  const signInWithGoogle = async () => {
    try {
      const res = await auth.signInWithPopup(googleProvider);
      const user = res.user;
      const query = await db
        .collection("users")
        .where("uid", "==", user.uid)
        .get();
      if (query.docs.length === 0) {
        await db.collection("users").add({
          uid: user.uid,
          name: user.displayName,
          authProvider: "google",
          email: user.email,
        });
      }
    } catch (err) {
      console.error(err);
      alert(err.message);
    }
  };

  //s'enregistrer sur la platforme 
  const registerWithEmailAndPassword = async (firstName, lastName, email, password, confirmationPassword) => {
    try {
      const res = await auth.createUserWithEmailAndPassword(email, password);
      const user = res.user;
      await db.collection("users").add({
        uid: user.uid,
        firstName,
        lastName,
        confirmationPassword,
        authProvider: "local",
        email,
      });
      alert("user enregistrer");
    } catch (err) {
      console.error(err);
      alert(err.message);
    }
  };

  //mofifier son mot de passe 
  const sendPasswordResetEmail = async (email) => {
    try {
      await auth.sendPasswordResetEmail(email);
      alert("Password reset link sent!");
    } catch (err) {
      console.error(err);
      alert(err.message);
    }
  };

  //se deconnecter 
  const logout = useCallback(() => {
    console.error("logOut");
    app.auth().signOut().then(() => {
      // dispatch(logout());
      // dispatch(cleanPanier());

    });
  }, [dispatch]);

  useEffect(() => {
    const unSubscribe = auth.onAuthStateChanged((user) => {
      if (user) {
        dispatch(logInWithEmailPassword({ email: user.email, name: undefined }));
      } else {
        dispatch(logOut());
        dispatch(cleanPanier());
      }
    });
    return unSubscribe;
  }, [dispatch])

  return { signInWithEmailAndPassword, signInWithGoogle, sendPasswordResetEmail, logout, registerWithEmailAndPassword }
}

export {
  auth,
  db,
};