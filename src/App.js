import { useSelector } from "react-redux";
import './App.css';
import BarreRecherche from './components/BarreRecherche/BarreRecherche';
import Sacs from './Image/Sac.png';
import Habits from './Image/Habits.png'
import Chaussures from './Image/Chaussures.png'
import Chemises from './Image/Chemises.png'
import Bijoux from './Image/Bijoux.png'
import { Grid } from '@material-ui/core';
import { getUserLog } from './redux/userSlice'

function App() {
  const userState = useSelector(getUserLog);
  return (
    <div className="App">
      <header className="App-header">
        <BarreRecherche />
      </header>
      <section>
        <div className="Bloc">
          <Grid
            container
            direction="row"
            justifyContent="space-evenly"
            alignItems="center"
            className="Bloc_habits">
            <grid>
              <img src={Habits} alt="habits" className="images" />
            </grid>
            <grid>
              <img src={Chemises} alt="chemises" className="images" />
            </grid>
            <grid>
              <img src={Chaussures} alt="chaussure" className="images" />
            </grid>
          </Grid>
          <Grid container
            direction="row"
            justifyContent="space-evenly"
            alignItems="center"
            className="Bloc_habits">
            <grid>
              <img src={Bijoux} alt="bijoux" className="images" />
            </grid>
            <grid>
              <img src={Sacs} alt="sacs" className="images" />
            </grid>
          </Grid>
        </div>
        {userState.email}
      </section>
      <footer className="footer">
        <div></div>
      </footer>
    </div>
  );
}

export default App;
