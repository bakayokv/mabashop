import React from "react";
import { AppBar, Toolbar, Button, withStyles, MenuItem, Menu, makeStyles } from "@material-ui/core";
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from "react-router-dom";
import { useFirebaseFunction } from "../../firebase";



const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    getContentAnchorE2={null}
    getContentAnchorE3={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: "#f59d1a",
      '& .MuiListItemText-primary': {
        color: '#223467',
      },
    },
  },
}))(MenuItem);

const Barre = () => {

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchorE2, setAnchorE2] = React.useState(null);
  const [anchorE3, setAnchorE3] = React.useState(null);

  const classes = useStyles();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleAnchor2 = (event) => {
    setAnchorE2(event.currentTarget);
  };

  const handleAnchor3 = (event) => {
    setAnchorE3(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setAnchorE2(null);
    setAnchorE3(null);
  };

  const { logout } = useFirebaseFunction();

  return (
    <div className={classes.root}>
      <AppBar style={{ background: '#223467' }} position="static">
        <Toolbar>
          <Button style={{ background: '#f59d1a' }}>Acceuil</Button>

          {/* Bouton femme ---------------*/}
          <Button aria-controls="customized-menu"
            aria-haspopup="true"
            onClick={handleClick}
            style={{ color: '#f59d1a', background: "#223467" }}>
            Femme
          </Button>
          <StyledMenu id="customized-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}>
            <Link to='/articleFemme'>
              <StyledMenuItem>
                <ListItemText primary="Habits" />
              </StyledMenuItem>
            </Link>
            <StyledMenuItem>
              <ListItemText primary="Chaussures" />
            </StyledMenuItem>
            <StyledMenuItem>
              <ListItemText primary="Robes" />
            </StyledMenuItem>
          </StyledMenu>

          {/* Bouton Homme ---------------*/}
          <Button aria-controls="customized-menu"
            aria-haspopup="true"
            onClick={handleAnchor2}
            style={{ color: '#f59d1a', background: "#223467" }}>
            Homme
          </Button>
          <StyledMenu id="customized-menu"
            anchorE2={anchorE2}
            keepMounted
            open={Boolean(anchorE2)}
            onClose={handleClose}>
            <Link to='/articlesHomme'>
              <StyledMenuItem>
                <ListItemText primary="Habits" />
              </StyledMenuItem>
            </Link>
            <StyledMenuItem>
              <ListItemText primary="Chaussures" />
            </StyledMenuItem>
            <StyledMenuItem>
              <ListItemText primary="Robes" />
            </StyledMenuItem>
          </StyledMenu>

          {/* Bouton Accessoire --------------- */}
          <Button aria-controls="customized-menu"
            aria-haspopup="true"
            onClick={handleAnchor3}
            style={{ color: '#f59d1a' }}
          >
            Accessoire
          </Button>
          <StyledMenu id="customized-accessoire"
            anchorE3={anchorE3}
            keepMounted
            open={Boolean(anchorE3)}
            onClose={handleClose}>
            <StyledMenuItem>
              <ListItemText primary="Colliers" />
            </StyledMenuItem>
            <StyledMenuItem>
              <ListItemText primary="Boucles " />
            </StyledMenuItem>
            <StyledMenuItem>
              <ListItemText primary="Montres" />
            </StyledMenuItem>
          </StyledMenu>

          <Link to='/login' style={{ color: '#f59d1a', display: "flex", justifyContent: "flex-end" }} className={classes.title}>
            <Button style={{ color: '#f59d1a' }}>Login</Button>
          </Link>
          <Button onClick={logout} style={{ color: '#f59d1a', display: "flex", justifyContent: "flex-end" }}>Logout</Button>

          <Link to='/panier' style={{ color: '#f59d1a', display: "flex", justifyContent: "flex-end" }} >
            <Button style={{ color: '#f59d1a' }}>Panier</Button>
          </Link>

        </Toolbar>
      </AppBar>
    </div>
  )
}





export default Barre;
