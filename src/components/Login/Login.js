import React from "react";
import "./Login.css";
import logo_connexion from "../../Image/iconelog.png";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button, Grid } from "@material-ui/core";
import traitBlanc from '../../Image/trait_blanc.png';
import google from '../../Image/google.png';
import yahoo from '../../Image/yahoo.png';
import facebook from '../../Image/facebook.png';
import { useState } from "react";
import { useFirebaseFunction } from "../../firebase";
import { getIsUserLogged } from "../../redux/userSlice";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";


const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '35ch',
      backgroundColor: "white",
      marginBottom: "3px",
      marginTop: "15px"
    },
  },
  input: {
    backgroundColor: "#f59d1a",
    color: "white",
    width: "20ch",
    display: "flex",
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '30px'
  },

  texteMdp: {
    color: "white",
    textAlign: 'center',
    marginTop: '10px',
  },
  grids: {
    flexGrow: 1,
  },
  texte: {
    color: "white",
    marginTop: '10px',
    marginLeft: "7px"
  }

}));



const Login = () => {


  const [email, setEmail] = useState("");
  const handleMail = event => {
    setEmail(event.target.value);
  };

  const [password, setPassword] = React.useState("");
  const handlePassword = event => {
    setPassword(event.target.value);
  };

  const [nom, setNom] = React.useState('');
  const handlenom = event => {
    setNom(event.target.value);
  };

  const [prenom, setPrenom] = React.useState('');
  const handlePrenom = event => {
    setPrenom(event.target.value);
  };

  const [emailEnregistrement, setEmailEnregistrement] = useState("");
  const handleMailEnregistrement = event => {
    setEmailEnregistrement(event.target.value);
  };

  const [passwordEnregistrement, setPasswordEnregistrement] = React.useState("");
  const handlePasswordEnregistrement = event => {
    setPasswordEnregistrement(event.target.value);
  };

  const [confirmPassword, setConfirmPassword] = React.useState('');
  const handleConfirmPassword = event => {
    setConfirmPassword(event.target.value);
  };

  const { signInWithEmailAndPassword, sendPasswordResetEmail, registerWithEmailAndPassword, signInWithGoogle } = useFirebaseFunction();

  const classes = useStyles();

  const isUserLogged = useSelector(getIsUserLogged);
  const history = useHistory();
  if (isUserLogged) {
    history.push("/");
  };

  return (
    <div className="login">
      <div>
        <img src={logo_connexion} alt="iconelog" className="iconelog" />
      </div>
      <div className="connection">
        < Grid container direction="row" justifyContent="center" alignItems="flex-start" >


          {/* Partie se connecter  */}
          <Grid style={{ margin: "20px 130px" }}>
            {/*Email */}
            <div>
              <form className={classes.root} noValidate autoComplete="off"  >
                <TextField id="filled-basic"
                  value={email}
                  onChange={handleMail}
                  label="NomPrenom@gmail.com"
                  variant="filled" />
                {console.log(email)}

              </form>
              <h7 className={classes.texte}>Nom d'utilisateur</h7>
            </div>
            {/*Mot de passe */}
            <div>
              <form className={classes.root} noValidate autoComplete="off"  >
                <TextField id="filled-basic"
                  value={password}
                  onChange={handlePassword}
                  label="BerengerBernie"
                  variant="filled" />
                {console.log(password)}
              </form>
              <h7 className={classes.texte}>Mot de passe</h7>
            </div>
            <div style={{ display: 'flex', flexDirection: "column" }}>
              <Button className={classes.input} onClick={() => signInWithEmailAndPassword(email, password)} type="submit">Se connecter</Button>

              {/*Mot de passe oublié */}
              <Button className={classes.texteMdp} onClick={() => sendPasswordResetEmail(email)}>Mot de passe oublié ?</Button>

              <div className="reseaux">
                <Grid container direction="row"
                  justifyContent="space-evenly"
                  alignItems="center">
                  <Grid>
                    <img src={google} onClick={signInWithGoogle} alt="gmail" style={{ width: "55px" }} />
                  </Grid>
                  <Grid>
                    <img src={facebook} alt="facebook" style={{ width: "60px" }} />
                  </Grid>
                  <Grid>
                    <img src={yahoo} alt="yahoo" style={{ width: "55px" }} />
                  </Grid>
                </Grid>
              </div>
            </div>
          </Grid>


          {/* Trait de delimitage  */}
          <Grid style={{ marginTop: "20px" }}>
            <img src={traitBlanc} alt="trait blanc" />
          </Grid>


          {/* Partie inscription  */}
          <Grid style={{ margin: "20px 130px" }}>
            {/*Nom */}
            <div>
              <form className={classes.root} noValidate autoComplete="off"  >
                <TextField id="filled-basic"
                  value={nom}
                  onChange={handlenom}
                  label="Berenger"
                  variant="filled" />
              </form>
              <h7 className={classes.texte} >Nom</h7>
            </div>
            {/*Prenom */}
            <div>
              <form className={classes.root} noValidate autoComplete="off"  >
                <TextField id="filled-basic"
                  value={prenom}
                  onChange={handlePrenom}
                  label="Bernie"
                  variant="filled" />
              </form>
              <h7 className={classes.texte} >Prenom</h7>
            </div>
            {/*Email */}
            <div>
              <form className={classes.root} noValidate autoComplete="off"  >
                <TextField id="filled-basic"
                  value={emailEnregistrement}
                  onChange={handleMailEnregistrement}
                  label="BerengerBernie@yahoo.fr"
                  variant="filled" />
              </form>
              <h7 className={classes.texte} >Email</h7>
            </div>
            {/*Mot de passe  */}
            <div>
              <form className={classes.root} noValidate autoComplete="off"  >
                <TextField id="filled-basic"
                  value={passwordEnregistrement}
                  onChange={handlePasswordEnregistrement}
                  label="BerengerBernie97*"
                  variant="filled" />
              </form>
              <h7 className={classes.texte}>Mot de passe</h7>
            </div>
            {/*Confirmation mot de passe  */}
            <div>
              <form className={classes.root} noValidate autoComplete="off"  >
                <TextField id="filled-basic"
                  value={confirmPassword}
                  onChange={handleConfirmPassword}
                  label="BerengerBernie97*"
                  variant="filled" />
              </form>
              <h7 className={classes.texte} >Confirmation mot de passe</h7>
            </div>

            <Button className={classes.input} onClick={() => registerWithEmailAndPassword(nom, prenom, emailEnregistrement, passwordEnregistrement, confirmPassword)} type="submit">S'enregistrer</Button>
          </Grid>

        </Grid>
      </div>

    </div>
  )
}

export default Login;
