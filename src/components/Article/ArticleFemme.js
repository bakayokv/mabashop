import BarreRecherche from '../BarreRecherche/BarreRecherche.js';
import { Grid, makeStyles } from "@material-ui/core";
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import * as React from 'react';
import Categorie from './Categorie.js';

const listesArticles = [
    {
        id: 1,
        marque: "A",
        taille: "DD",
        desc: "",
        Couleur: "Mama",
        avis: "Vava",
        quantitee: 5,
        prix: "5£",
        Images: {
            image1: "im1.png",
            image2: "im2.png",
            image3: "im3.png",
            image4: "im4.png"
        },
    },
    {
        id: 2,
        marque: "B",
        taille: "DD",
        desc: "",
        Couleur: "Mama",
        avis: "Vava",
        quantitee: 5,
        prix: "5£",
        Images: {
            image1: "im1.png",
            image2: "im2.png",
            image3: "im3.png",
            image4: "im4.png"
        },
    },
    {
        id: 2,
        marque: "B",
        taille: "DD",
        desc: "",
        Couleur: "Mama",
        avis: "Vava",
        quantitee: 5,
        prix: "5£",
        Images: {
            image1: "im1.png",
            image2: "im2.png",
            image3: "im3.png",
            image4: "im4.png"
        },
    },
    {
        id: 2,
        marque: "B",
        taille: "DD",
        desc: "",
        Couleur: "Mama",
        avis: "Vava",
        quantitee: 5,
        prix: "5£",
        Images: {
            image1: "im1.png",
            image2: "im2.png",
            image3: "im3.png",
            image4: "im4.png"
        },
    },
    {
        id: 2,
        marque: "B",
        taille: "DD",
        desc: "",
        Couleur: "Mama",
        avis: "Vava",
        quantitee: 5,
        prix: "5£",
        Images: {
            image1: "im1.png",
            image2: "im2.png",
            image3: "im3.png",
            image4: "im4.png"
        },
    },
    {
        id: 2,
        marque: "B",
        taille: "DD",
        desc: "",
        Couleur: "Mama",
        avis: "Vava",
        quantitee: 5,
        prix: "5£",
        Images: {
            image1: "im1.png",
            image2: "im2.png",
            image3: "im3.png",
            image4: "im4.png"
        },
    },

]

const useStyles = makeStyles((theme) => ({
    articleStyle: {
        backgroundColor: "#f59d1a",
        flexDirection: "column",
        justifyContent: "center",
        textAlign: "center",
        margin: "auto",
        marginBottom: "10px"
    },
    root: {
        padding: "20px"
    },

    bloc: {
        margin: "10px",
        border: " 1px solid #223467",
    },

    elt: {
        textAlign: "center",
    },

    image: {
        flexDirection: "column",
        justifyContent: "center",
        textAlign: "center",
        margin: "auto",
    }
}));



const ArticleFemme = () => {

    const classes = useStyles();

    const [page, setPage] = React.useState(1);
    const handleChange = (event, value) => {
        setPage(value);
    };

    return (
        <div>
            <BarreRecherche />
            Femme
            <Grid container>
                <Grid xs={2}>
                    <Categorie />
                </Grid >
                <Grid xs={10} style={{ minHeight: "93vh" }}>
                    <Grid container xs={12} className={classes.root} justifyContent="center">
                        {listesArticles.map((item) => (
                            <Grid item xs={2} className={classes.bloc} >
                                {/*Try to use stack */}
                                <Grid className={classes.image}>
                                    <img alt="Images" src={item.image1} />
                                </Grid>
                                <Grid className={classes.elt}>
                                    {item.marque}
                                </Grid >
                                <Grid xs={9} className={classes.elt}>
                                    {item.desc}
                                </Grid>
                                <Grid xs={9} className={classes.articleStyle}>
                                    {item.prix}
                                </Grid>
                            </Grid>
                        ))}
                    </Grid>
                    {/* Integrate router pagination*/}
                    <Grid container xs={10} className={classes.root} justifyContent="center">
                        <Stack spacing={2}>
                            <Typography>Page: {page}</Typography>
                            <Pagination count={10} page={page} onChange={handleChange} />
                        </Stack>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

export default ArticleFemme