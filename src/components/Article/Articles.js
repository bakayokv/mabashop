import { putInPanier } from '../../redux/panierSlice.js';
import BarreRecherche from '../BarreRecherche/BarreRecherche.js';
import { useDispatch } from 'react-redux';

import { Grid, Button } from "@material-ui/core"

const listesArticles = [
    {
        id: 1,
        marque: "A",
        taille: "DD",
        desc: "",
        Couleur: "Mama",
        avis: "Vava",
        quantitee: 5,
        prix: "5£",
        Images: {
            image1: "im1.png",
            image2: "im2.png",
            image3: "im3.png",
            image4: "im4.png"
        },
    },
    {
        id: 2,
        marque: "B",
        taille: "DD",
        desc: "",
        Couleur: "Mama",
        avis: "Vava",
        quantitee: 5,
        prix: "5£",
        Images: {
            image1: "im1.png",
            image2: "im2.png",
            image3: "im3.png",
            image4: "im4.png"
        },
    },

]



const EnsembleArticle = () => {
    const dispatch = useDispatch();

    const onDispacth = (props) => {
        dispatch(putInPanier(props));
    }

    return (
        <div>
            <BarreRecherche />
            <Grid>
                <Grid>
                    {listesArticles.map((item) => (
                        <Grid>
                            <Grid>
                                <div>
                                    <div>
                                        <img alt="Images" src={item.image1} />
                                    </div>
                                    <h3>{item.marque}</h3>
                                    <h6>
                                        {item.desc}
                                        <br />{item.prix}
                                        <br />{item.taille}
                                        <br />{item.avis}
                                    </h6>
                                </div>
                            </Grid>
                            <Button onClick={() => onDispacth(item)}>Ajouter Panier</Button>
                        </Grid>
                    )
                    )}
                </Grid>
            </Grid>
        </div>
    );
}

export default EnsembleArticle