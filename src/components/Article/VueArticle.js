import React from 'react';
import { Grid } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { getView } from '../../redux/vueArticleSlice.js';

export default function VueArticle() {

    const viewState = useSelector(getView);

    return (
        <div>
            {viewState.map((item) => (
                <Grid>
                    <section>
                        <Grid>
                            <Grid>
                                <img alt="Images" src={item.image1} />
                            </Grid>
                            <Grid>
                                <Grid>{item.marque} </Grid>
                                <Grid> {item.taille} </Grid>
                                <Grid> {item.quantitee} </Grid>
                                <Grid> {item.avis} </Grid>
                            </Grid>
                        </Grid>
                    </section>
                </Grid>
            ))}
        </div>
    )
}



