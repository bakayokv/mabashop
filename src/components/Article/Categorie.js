import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Checkbox from '@mui/material/Checkbox';
import { FormControlLabel } from '@mui/material';

function Categorie() {

    const [expanded, setExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    return (
        <div style={{ backgroundColor: "#223467", minHeight: "100vh" }}>
            <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')} style={{ backgroundColor: "#223467", color: "#eb981d" }}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1bh-content"
                    id="panel1bh-header">
                    <Typography sx={{ width: '33%', flexShrink: 0 }}>
                        Categorie
                    </Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <FormControlLabel label="Chaussures"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                    <br />
                    <FormControlLabel label="Habits"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                    <br />
                    <FormControlLabel label="Pantalons"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')} style={{ backgroundColor: "#223467", color: "#eb981d" }}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2bh-content"
                    id="panel2bh-header"
                >
                    <Typography sx={{ width: '33%', flexShrink: 0 }}>Taille</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <FormControlLabel label="L"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                    <br />
                    <FormControlLabel label="M"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                    <br />
                    <FormControlLabel label="S"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')} style={{ backgroundColor: "#223467", color: "#eb981d" }}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel3bh-content"
                    id="panel3bh-header"
                >
                    <Typography sx={{ width: '33%', flexShrink: 0 }}>
                        Prix
                    </Typography>

                </AccordionSummary>
                <AccordionDetails>
                    <FormControlLabel label=" prix < 50£"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                    <br />
                    <FormControlLabel label="prix > 50£"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === 'panel4'} onChange={handleChange('panel4')} style={{ backgroundColor: "#223467", color: "#eb981d" }}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel4bh-content"
                    id="panel4bh-header"
                >
                    <Typography sx={{ width: '33%', flexShrink: 0 }}>Materiel</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <FormControlLabel label="Coton"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                    <br />
                    <FormControlLabel label="Cuire"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                    <br />
                    <FormControlLabel label="Nillon"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === 'panel5'} onChange={handleChange('panel5')} style={{ backgroundColor: "#223467", color: "#eb981d" }}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel4bh-content"
                    id="panel4bh-header"
                >
                    <Typography sx={{ width: '33%', flexShrink: 0 }}>Couleur</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <FormControlLabel label="Bleu"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                    <br />
                    <FormControlLabel label="Blanc"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                    <br />
                    <FormControlLabel label="Rouge"
                        control={<Checkbox defaultChecked size="small" />}
                    />
                </AccordionDetails>
            </Accordion>
        </div>
    )
}

export default Categorie;

