import { Grid, Button, makeStyles } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import { getPanier } from '../../redux/panierSlice';
import { useDispatch } from 'react-redux';
import { putInSousPanier } from '../../redux/sousPanierSlice.js';
import SousPanier from './SousPanier';
import Checkbox from '@mui/material/Checkbox';
import Box from '@mui/material/Box';
import HorizontalRuleIcon from '@mui/icons-material/HorizontalRule';
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';
import RemoveOutlinedIcon from '@mui/icons-material/RemoveOutlined';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    article: {
        width: "90%",
    },
    centerBlock: {
        display: "flex",
        justifyContent: "left"
    },
    lineBlock: {
        display: "flex",
        flexDirection: "row",
        width: "100%",
    }
}));

const commonStyles = {
    borderColor: "yellow",
    height: "180px",
    width: "70%",
    display: "flex",
    justifyContent: "left",
};

const commonStyles_2 = {
    bgcolor: 'background.paper',
    borderColor: "yellow",
    marginLeft: "30px",
};

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

export default function Panier() {
    const panierState = useSelector(getPanier);
    const dispatch = useDispatch();

    const onDispacth = (props) => {
        dispatch(putInSousPanier(props));
    }

    const classes = useStyles();


    return (
        <div>
            <Box sx={{ ...commonStyles_2, border: 1 }}>
                Message important : il est impossible pour l'instant de faire des achats en ligne.
                Ceci est du au fait qu'un grand nombre de personne en  Cote d'ivoire ne possede pas
                de carte de credit. Il est seulement possible d'enregistrer les articles vous
                plaisant et aller les acheter directement en magasin. Je vous remercie pour votre
                comprehension
            </Box>
            <Grid style={{ width: "70%", marginLeft: "2%" }}>
                <h3>Votre Panier</h3>
                <h6>selectionner tous les elements</h6>
                <br />

                {panierState.map((item) => (
                    <Grid className={classes.centerBlock} >
                        <Box className={classes.article} sx={{ ...commonStyles, borderTop: 1, borderBottom: 1 }}>
                            <Grid className={classes.lineBlock}>
                                <Grid style={{ marginTop: "auto", marginBottom: "auto" }} >
                                    <Checkbox {...label} />
                                </Grid>
                                <Grid style={{
                                    backgroundColor: "blue",
                                    width: "150px",
                                    height: "150px",
                                    marginTop: "auto",
                                    marginBottom: "auto"
                                }}>
                                    <div ></div>
                                </Grid>
                                <Grid style={{
                                    marginLeft: "20px",
                                    display: "flex",
                                    flexDirection: "column",
                                    justifyContent: "center",
                                    paddingTop: "10px",
                                    paddingBottom: "10px"
                                }}>
                                    <Grid>{item.marque} </Grid>
                                    <Grid> {item.taille} </Grid>
                                    <Grid> {item.quantitee} </Grid>
                                    <Grid> {item.avis} </Grid>
                                    <Grid>
                                        <Button onClick={() => onDispacth(item)}>
                                            Mettre de coté
                                        </Button>
                                        <Button style={{ color: "red" }}>
                                            Supprimer
                                        </Button>
                                    </Grid>
                                </Grid>
                                <Grid style={{
                                    marginTop: "auto",
                                    paddingRight: "30px",
                                    marginBottom: "auto", marginLeft: "auto", display: "flex", flexDirection: "column", justifyContent: "right"
                                }}>
                                    <div>
                                        Quantité: 2
                                    </div>
                                    {/*Icone du Plus */}
                                    <Box style={{ backgroundColor: "yellow", width: "30px" }} className={classes.centerBlock}>
                                        <AddOutlinedIcon />
                                    </Box>
                                    <Box>
                                        <HorizontalRuleIcon style={{ width: "30px" }} className={classes.centerBlock} />
                                    </Box>
                                    <Box style={{ backgroundColor: "yellow", width: "30px" }} className={classes.centerBlock}>
                                        <RemoveOutlinedIcon />
                                    </Box>
                                    {/*Icone barre horizontale */}
                                    {/*Icone du moins */}
                                    <div>
                                        prix: 25£
                                    </div>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>

                ))
                }
            </Grid >

            <Grid>
                <SousPanier />
            </Grid>
        </div >
    )
}
