import { Grid, Button } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import { getSousPanier } from '../../redux/sousPanierSlice';

export default function SousPanier() {
    const sousPanierState = useSelector(getSousPanier);

    return (
        <div>
            <Grid>
                <h3>Sous Panier</h3>
                <h6>selectionner tous les elements</h6>
                <br />

                {sousPanierState.map((item) => (
                    <Grid>
                        <section>
                            <hr />
                            <Grid>
                                <Grid>
                                    <img alt="Images" src={item.image1} />
                                </Grid>
                                <Grid>
                                    <Grid>{item.marque} </Grid>
                                    <Grid> {item.taille} </Grid>
                                    <Grid> {item.quantitee} </Grid>
                                    <Grid> {item.avis} </Grid>
                                    <Grid>
                                        <Button>
                                            Supprimer
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </section>

                    </Grid>
                ))}
                <hr />
            </Grid>

        </div>
    )
}
