import { createSlice } from '@reduxjs/toolkit'

const initialState = []

const vuePanierSlice = createSlice({
    name: 'vuePanier',
    initialState,
    reducers: {
        putInVew: (state, action) => {
            state.push(action.payload);
        }
    }
})

export const getView = (state) => state.vuePanier; //exporter le nom
export const { putInVew } = vuePanierSlice.actions; // exporter les actions de notre vue
export default vuePanierSlice.reducer; // exporter notre reducer