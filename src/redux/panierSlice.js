import { createSlice } from '@reduxjs/toolkit'


const initialState = [];
// strore : ensemble de slice [userSlice ...]

const userPanierSlice = createSlice({
    //nom
    name: 'panier',
    //etat
    initialState,
    //action qui vont changer l'etat : reducers.

    //reducer
    reducers: {
        putInPanier: (state, action) => {
            state.push(action.payload);
        },
        cleanPanier: (state) => {
            state.splice(0, state.length);
        },
    }
})

export const getPanier = (state) => state.panier;
export const { putInPanier, cleanPanier } = userPanierSlice.actions; // peut etre appelé dans n'importe quel composant 
export default userPanierSlice.reducer;

