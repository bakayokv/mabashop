import { createSlice } from '@reduxjs/toolkit'


const initialState = [];
// strore : ensemble de slice [userSlice ...]

const userSousPanierSlice = createSlice({
    //nom
    name: 'sousPanier',
    //etat
    initialState,
    //action qui vont changer l'etat : reducers.

    //reducer
    reducers: {
        putInSousPanier: (state, action) => {
            state.push(action.payload);
        },
        cleanPanier: (state) => {
            state.splice(0, state.length);
        },
        removeItem: (state, action) => {

        }
    }
})

export const getSousPanier = (state) => state.sousPanier;
export const { putInSousPanier } = userSousPanierSlice.actions; // peut etre appelé dans n'importe quel composant 
export default userSousPanierSlice.reducer;

