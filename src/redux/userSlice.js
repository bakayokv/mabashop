import { createSlice } from '@reduxjs/toolkit'


const initialState = {
    name: undefined,
    email: undefined,
}
// strore : ensemble de slice [userSlice ...]

const userSlice = createSlice({
    //nom
    name: 'user',
    //etat
    initialState,
    //action qui vont changer l'etat : reducers.

    //reducer
    reducers: {
        logOut: (state) => {
            state.email = initialState.email;
            state.name = initialState.name;
        },
        logInWithEmailPassword: (state, action) => {
            state.email = action.payload.email;
            state.name = action.payload.name;
        },
    }
})

export const getUserLog = (state) => state.user;
export const getIsUserLogged = (state) => Boolean(state.user.email);
export const { logOut, logInWithEmailPassword } = userSlice.actions; // peut etre appelé dans n'importe quel composant 
export default userSlice.reducer;

