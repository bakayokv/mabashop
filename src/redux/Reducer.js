import { combineReducers } from 'redux';
import userReducer from './userSlice';
import userPanierSlice from './panierSlice';
import userSousPanierSlice from './sousPanierSlice';
import userViewPanierSlice from './vueArticleSlice';



//ensemble de tous les reducers 
export default combineReducers({
    user: userReducer, // mise en relation entre slice(userReducer) + Selector(user)
    panier: userPanierSlice,
    sousPanier: userSousPanierSlice,
    vuePanier: userViewPanierSlice
})